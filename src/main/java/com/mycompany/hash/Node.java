/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hash;

/**
 *
 * @author informatics
 */
public class Node {
    private int key;
    private String value;
    private String[] arrk = new String [128];

    public String get(int key) {
        return arrk[key];
    }
    
    public void put(int key, String value) {
        arrk[Hash(key)] = value;
    }
    
    public void remove(int key) {
        arrk[Hash(key)] = null;
    }
    
    public int Hash(int key) {
        return key % arrk.length;
    }
}
